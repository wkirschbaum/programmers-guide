.. programmers-guide documentation master file, created by
   sphinx-quickstart on Mon Feb 19 19:34:01 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Programmers guide
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   guides/index
   bugs/index

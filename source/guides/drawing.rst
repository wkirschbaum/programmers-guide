Drawing test
==================

.. graphviz::

   digraph foo {
      a [label="sphinx", href="http://sphinx-doc.org", target="_top"]
      game [label="games"]
      
      "bar" -> "baz" [ label="yes" ];
      "zar" -> "car" -> "yarn";
      "art" -> "part" -> "bar" -> "zar";
      "yarn" -> "part" -> game;
      "yarn" -> "art" -> a [ label="no" ];
   }

.. math::
  :nowrap:

   \begin{eqnarray}
      y    & = & ax^2 + bx + c \\
      f(x) & = & x^2 + 2xy + y^2
   \end{eqnarray}

.. code-block:: ruby

  def something
    puts "word"
  end
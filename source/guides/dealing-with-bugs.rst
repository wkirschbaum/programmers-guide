Dealing with bugs
=============================================

Bugs can either be found when reviewing a **honeybadger**, 
**customer communication** or doing **explorative testing**.

When a potential bug is identified follow these steps in order

* Communicate the possible bug to the team
* Verify the bug either on the staging environment or inspecting the code
* Determine the urgency of the bug

The :doc:`urgency <../bugs/urgency>` of the bug will determine if the fix should be :doc:`ignored <../bugs/actions>`, 
:doc:`planned <../bugs/actions>` or
:doc:`prioritised <../bugs/actions>` within the sprint.

If the bug is **user facing**

* Write a **feature spec** to reproduce the error
* Ensure that the spec is failing as reported
* Fix the bug with the **least amount of changes**
* Ensure the spec is passing
* Deploy the fix as per the team's release agreement
* Resolve the issue on Honeybadger if required
* Log a ticket for a complete fix if required
* Report the change and the future work with your team at the next standup

If the bug is **system only**

* Write a **integration test** to reproduce the error
* Ensure that the test is failing as observed
* Fix the bug with the **least amount of changes**
* Ensure the test is passing
* Deploy the fix as per the team's release agreement
* Resolve the issue on Honeybadger if required
* Log a ticket for a complete fix if required
* Report the change and the future work with your team at the next standup
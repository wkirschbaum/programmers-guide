Determining the bug urgency
=============================================

The urgency is determined based on one of the following situations. 
Low urgency is for the scenarios not listed here. This is only a guideline, 
so each issue should be discussed as per the :doc:`../guides/dealing-with-bugs` guide.

Critical:

* Student or Investor registrations are blocked
* Student application process is blocked
* Discoverability of the main domain sites are hampered
* Existing data is being compromised

High:

* Students or Investors are receiving error pages
* Ops is experience difficulty progressing students
* Any user is experience degraded application performance
* Bad data is being generated


Medium:

* A small group of users is experiencing anomalies
* Ops is experiencing an issue with a possible work-around
* A non-critical feature is not functioning as expected


Actions to take for bugs
=============================================

These actions can be taken when identifying a bug. Ensure to follow the :doc:`../guides/dealing-with-bugs`
before deciding on an action.

Ignore:

Do not resolve the issue on Honeybadger. Ignored issues will have to stay visible
so that we can review if they should stay ignored. In extreme cases there is the
option to mark them as ignored on Honeybadger, but this is discouraged as it will
remove visibility

Plan:

If a bug has an :doc:`urgency <../bugs/urgency>` of Medium or lower, then it should be part of the next planning meeting

Prioritise:

If a bug has an :doc:`urgency <../bugs/urgency>` of High or Cricial, then it should be prioritised with the team
as soon as possible.